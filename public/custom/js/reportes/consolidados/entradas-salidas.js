$(document).ready(function(){
	$("#grafico").hide();
})



//Inicializando calendario
$("#rango-consulta").on('focus', function(e) {
	e.preventDefault();
	$("#rango-consulta").daterangepicker({
		showDropdowns: true,
		maxDate: fecha(),
		locale: {
			format: 'DD/MM/YYYY',
			daysOfWeek: [
				"Do",
				"Lu",
				"Ma",
				"Mi",
				"Ju",
				"Vi",
				"Sa"
			],
			monthNames: [
				"Enero",
				"Febrero",
				"Marzo",
				"Abril",
				"Mayo",
				"Junio",
				"Julio",
				"Agosto",
				"Septiembre",
				"Octubre",
				"Noviembre",
				"Diciembre"
			]
		}
	});
});

// $(document).on('change', '#tipo-operacion', function(e) {
//     let mode= $('#tipo-operacion option:selected').val();
//    if (mode=='3') {
// 	$("#grafico").hide();
//    }

// });

//Evento de envio de formulario al controlador
$(document).on('submit', "#consulta-articulos", function(e) {
	e.preventDefault();
	let fechas = $("#rango-consulta").val().split(" - ");
	let datos = {
		"date_init" : invertirFecha(fechas[0]),
		"date_end"  : invertirFecha(fechas[1]),
		"mode"      : $("#tipo-operacion").val()
	}
	let modo =$("#tipo-operacion").val();
	let accion='';
	if (modo=='1') {
		accion='RelacionEntradas';						
	}
	else if (modo=='2') {
		accion='RelacionSalidas';						
	}
	else if (modo=='3') {
		accion='RelacionEntradas-Salidas';						
	}
	fechas_convetida = fechas.map(date => (moment(date, 'MM/DD/YYYY', true).isValid()) ? moment(date, 'MM/DD/YYYY', true).format('DD-MM-YYYY') : '');
	let mensaje = `Desde ${fechas_convetida[0]} hasta ${fechas_convetida[1]}`;
	$.ajax({
		url    : "/obtenerConsolidado",
		method : "POST",
		data : {"data" : btoa(JSON.stringify(datos))},
		beforeSend: function(){
			$("button[type=submit]").attr('disabled');
		}
	}).then((response) => {
		//alert(response);
		let modo=$("#tipo-operacion").val();
		if(modo=='1' || modo=="2"){
			$("#tabla").html(response.tabla);
			$("#2").hide();
			$("#1").show();
			$("#tabla").dataTable().fnDestroy();
			$('#tabla').DataTable({
				dom: "Blfrtip",
				buttons: {
				  dom: {
					button: {
					  className: 'btn-xs-xs'
					},
				  },
				  buttons: [{
					extend: "excel",
					text: 'Excel',
					className: 'btn-xs btn-dark',
					title: accion + ' ' + mensaje,
					download: 'open',
					exportOptions: {
					  columns: [0, 1, 2],
					},
					excelStyles: {
					  "template": [
						"blue_medium",
						"header_blue",
						"title_medium"
					  ]
					},
			  
				  }]
				},
				// Opciones de DataTables
				"language": {
				  "sProcessing": "Procesando...",
				  "sLengthMenu": "Mostrar _MENU_ registros",
				  "sZeroRecords": "No se encontraron resultados",
				  "sEmptyTable": "Ningún dato disponible en esta tabla",
				  "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				  "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
				  "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
				  "sInfoPostFix": "",
				  "sSearch": "Buscar:",
				  "sUrl": "",
				  "sInfoThousands": ",",
				  "sLoadingRecords": "Cargando...",
				  "oPaginate": {
					"sFirst": "Primero",
					"sLast": "Último",
					"sNext": "Siguiente",
					"sPrevious": "Anterior"
				  },
				  "oAria": {
					"sSortAscending": ": Activar para ordenar la columna de manera ascendente",
					"sSortDescending": ": Activar para ordenar la columna de manera descendente"
				  },
				  "columnDefs": [{
					"targets": [0],
					"visible": false,
					"searchable": false
				  }, ]
				},
				"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
				"sLengthMenu": "Mostrar _MENU_ registros"
			  });
		}else{
			
			$("#tabla2").html(response.tabla);
			$("#1").hide();
			$("#2").show();
			$("#tabla2").dataTable().fnDestroy();
			$('#tabla2').DataTable({
				dom: "Blfrtip",
				buttons: {
				  dom: {
					button: {
					  className: 'btn-xs-xs'
					},
				  },
				  buttons: [{
					extend: "excel",
					text: 'Excel',
					className: 'btn-xs btn-dark',
					title: accion + ' ' + mensaje,
					download: 'open',
					exportOptions: {
					  columns: [0, 1, 2,3],
					},
					excelStyles: {
					  "template": [
						"blue_medium",
						"header_blue",
						"title_medium"
					  ]
					},
			  
				  }]
				},
				// Opciones de DataTables
				"language": {
				  "sProcessing": "Procesando...",
				  "sLengthMenu": "Mostrar _MENU_ registros",
				  "sZeroRecords": "No se encontraron resultados",
				  "sEmptyTable": "Ningún dato disponible en esta tabla",
				  "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				  "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
				  "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
				  "sInfoPostFix": "",
				  "sSearch": "Buscar:",
				  "sUrl": "",
				  "sInfoThousands": ",",
				  "sLoadingRecords": "Cargando...",
				  "oPaginate": {
					"sFirst": "Primero",
					"sLast": "Último",
					"sNext": "Siguiente",
					"sPrevious": "Anterior"
				  },
				  "oAria": {
					"sSortAscending": ": Activar para ordenar la columna de manera ascendente",
					"sSortDescending": ": Activar para ordenar la columna de manera descendente"
				  },
				  "columnDefs": [{
					"targets": [0],
					"visible": false,
					"searchable": false
				  }, ]
				},
				"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
				"sLengthMenu": "Mostrar _MENU_ registros"
			  });
	 }	
	}).catch((request) => {
		Swal.alert("Error", request.messageJSON.message, "error");
	});
});

//Generacion de archivo csv 
$(document).on('click', "#generaArchivoExcel", function(e){
	e.preventDefault();
	let fechas = $("#rango-consulta").val().split(" - ");
	window.location = '/generarConsolidadoExcel/' + invertirFecha(fechas[0]) + '/' + invertirFecha(fechas[1]) + '/' + $("#tipo-operacion").val();
})