
$('.custom-range').ionRangeSlider({
	step:1,
	prettify: true,
});

$("#aprobar-orden").on("submit", function(e){
	e.preventDefault();
	//Arreglo para los id de los items
	let items = [];
	//Obtenemos toda la data del form
	let form = $(this).serialize().split("&");
	//Numero de orden
	let numorden = "";
	//Usuario que lo solicita
	let ususol = "";
	//Recorremos el form
	for(i = 0; i < form.length; i++){
		let buff = form[i].split("=");
		if(i === 0){
			numorden = buff[1];
		}
		else if(i === 1){
			ususol = buff[1];
		}
		else{
			items.push({itemid: buff[0], numuniap: buff[1]});
		}
	}
	//Objeto para convertir en JSON en la solicitud AJAX
	let data = {
		"numorden" : numorden,
		"ususol"   : ususol,
		"items"    : items,
	};
	const opciones = [
		{
		  id: 1,
		  label: 'Opción 1',
		  requiereCantidad: true,
		},
		{
		  id: 2,
		  label: 'Opción 2',
		  requiereCantidad: false,
		},
	  ];
	  function confirmarAccion(resultado) {
		if (resultado.isConfirmed) {
		  if (mostrarSelect) {
			// Mostrar el select y procesar la selección
			const opcionSeleccionada = opciones.find(opcion => opcion.id === resultado.value);
			if (opcionSeleccionada.requiereCantidad) {
			  // Validar cantidad
			  if (!resultado.inputValue || isNaN(resultado.inputValue)) {
				Swal.fire({
				  icon: 'error',
				  title: 'Error',
				  text: 'Debe ingresar una cantidad válida.',
				});
				return;
			  }
			  // Procesar la cantidad
			  console.log('Procesando cantidad:', resultado.inputValue);
			} else {
			  // No se requiere cantidad
			  console.log('No se requiere cantidad.');
			}
		  } else {
			// Mostrar un mensaje de error
			Swal.fire({
			  icon: 'error',
			  title: 'Error',
			  text: 'Debe seleccionar una opción antes de aprobar.',
			});
		  }
		}
	  }
	//Hacemos la solicitud AJAX
	Swal.fire({
		title: 'Confirmacion',
		text: "¿Está seguro de aprobar la orden?",
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Aprobar',
		cancelButtonText: 'Cancelar',
		
	}).then((result) => {
		if (result.value) {
			Swal.fire({
			text: "Seleccione Proevedor",
			input: 'select',
			inputOptions: opciones,
			inputPlaceholder: 'Seleccione una opción',
			showLoaderOnConfirm: true,
			preConfirm: confirmarAccion,
			allowOutsideClick: false,
		})
			// $.ajax({
			// 	url:'/nuevaorden',
			// 	method:"POST",
			// 	data: {"data" : btoa(JSON.stringify(data))},
			// 	dataType: "JSON",
			// }).then(function(response){
			// 	Swal.fire({ 
			// 		title: '¡Exito!', 
			// 		text: 'Orden Aprobada Exitosamente', 
			// 		icon: 'success', 
			// 		confirmButtonText: 'Aceptar',
			// 		preConfirm: function(data){
			// 			window.location = "/listarequisiciones";
			// 		}
			// 	});

			// }).catch(function(request){
			// 	Swal.fire({ title: '¡Error!', text: 'Orden ya aprobada', icon: 'Error', confirmButtonText: 'Aceptar'});
			// });
		}
		else{
			Swal.fire('Atencion', 'No se ha aprobado la orden','warning');
		}
	});
	
	$("button[type=submit]").removeAttr('disabled');
});