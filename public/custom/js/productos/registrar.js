let borrado = $("#borrado_actual").val();
if (borrado == '0') {
    $("#borrado").prop("checked", true);
} else {
    $("#borrado").prop("checked", false);
}

//******METODO QUE TOMA EL ESTATUS ACTUAL DEL CHECKBO*****
$('#borrado').click(function() {
    if ($('#borrado').is(':checked')) {

        $("#borrado_actual").val('0');
    } else {
        $("#borrado_actual").val('1');

    }

});

$(document).on('submit', '#nuevoproducto', function(e) {
    e.preventDefault();
    let buscar_codbar = $("#codbar").val();
    let borrado = $("#borrado_actual").val();
    var form = {
        'modform': $("#modform").val(),
        'codbar': $("#codbar").val(),
        'prodmar': $("#prodmar").val(),
        'prodmodel': $("#prodmodel").val(),
        'borrado': $("#borrado_actual").val()
    };
    //METODO PARA VERIFICAR QUE EL PRODUCTO NO TENGA EXISTENCIAS ANTES DE HACER EL BORRADO LOGICO 
    $.ajax({
        url: '/buscar_producto_existencias/' + buscar_codbar,
        method: 'POST',
        data: { 'data': btoa(JSON.stringify(form)) },
        dataType: 'JSON',
        beforeSend: function() {

        },
        success: function(data) {
            if (data == '') {
                //NUEVO PRODUCTO
                let buscar_codbar = $("#codbar").val();
                $.ajax({
                    url: '/addproduct',
                    method: 'POST',
                    data: { 'data': btoa(JSON.stringify(form)) },
                    dataType: 'JSON',
                    beforeSend: function() {
                        $("button[type=submit").attr('disabled', 'true');
                    },
                    success: function(response) {
                        window.location = '/consultaproducto'
                    },
                    error: function(request) {
                        Swal.fire('Error!', request.responseJSON.message, 'error');

                    }
                });

            } else if (data[0].numexis > 0 && borrado == 1) {

                //EL PRODUCTO YA EXISTE Y SE VA A ACTUALIZAR 
                alert('No se puede eliminar el producto debido a que tiene existencia en inventario')
            } else {

                $.ajax({
                    url: '/addproduct',
                    method: 'POST',
                    data: { 'data': btoa(JSON.stringify(form)) },
                    dataType: 'JSON',
                    beforeSend: function() {
                        $("button[type=submit").attr('disabled', 'true');
                    },
                    success: function(response) {
                        window.location = '/consultaproducto'
                    },
                    error: function(request) {
                        Swal.fire('Error!', request.responseJSON.message, 'error');

                    }
                });

            }

        }

    });

});