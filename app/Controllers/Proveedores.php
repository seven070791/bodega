<?php namespace App\Controllers;

use App\Models\Proveedores_model;
use CodeIgniter\API\ResponseTrait;

class Proveedores extends BaseController{

	use ResponseTrait;

	public function nuevo(){
		if($this->request->isAJAX() && $this->session->get('logged')){
			$datos = json_decode(utf8_encode(base64_decode($this->request->getPost('data'))), TRUE);
			$model = new Proveedores_model();
			$query = $model->nuevoProveedor($datos);
			if($query){
				return $this->respond(array('message' => 'registrado exitosamente', 200 ));
			}
			else{
				return $this->respond(array('message' => 'ha ocurrido un error', 500 ));
			}
		}
		else{
			return redirect()->to('/403');
		}
	}

	public function consulta(){
		if($this->session->get('logged')){
			$model = new Proveedores_model();
			$query = $model->getAll();
			$rows = array();
			$headings = array("Nº", "RIF", "Nombre", "Telefono Principal", "Telefono Secundario", "Acciones");
			if($query->resultID->num_rows > 0){
				foreach($query->getResult() as $row){
					$rows[] = array($row->idprov, $row->numrif, utf8_decode($row->nomprov), $row->telef1, $row->telef2, '<button class="btn btn-sm detalles btn-light" id="'.$row->idprov.'">Detalles</button><button class="btn btn-sm editar btn-primary" id="'.$row->idprov.'">Editar</button>');
				}
			}
			else{
				$rows[] = array('<td colspan="6">Sin registros</td>','','','','','');
			}
			$tabla = base64_encode($this->generarTabla($headings, $rows));
			echo view('template/header');
			echo view('template/nav_bar');
			echo view('proveedores/all_provider', array('tbody' => $tabla));
			echo view('template/footer');
			echo view('proveedores/footer');
		}
		else{
			return redirect()->to('/403');
		}
	}

	public function detalleProveedor(){
		if($this->request->isAJAX() && $this->session->get('logged')){
			$id = $this->request->getPost('data');
			$model = new Proveedores_model();
			$query = $model->getSingle($id);
			$data = array();
			if($query->resultID->num_rows > 0){
				foreach($query->getResult() as $row){
					$data['idprov']  = $row->idprov;
					$data['nomprov'] = $row->nomprov;
					$data['telef1'] = $row->telef1;
					$data['telef2'] = $row->telef2;
					$data['direccprov'] = $row->direccprov;
					$data['contemail'] = $row->email;
					$data["rifprov"] = $row->numrif;
				}
				return $this->respond(array('message' => 'success', 'data' => $data), 200);
			}
			else{
				return $this->respond(array('message' => 'not found'), 404);	
			}
		}
		else{
			return redirect()->to('/403');
		}
	}

	public function buscarProveedor(){
		if($this->request->isAJAX()){
			$datos = json_decode(base64_decode($this->request->getPost('data')), TRUE);
			$model = new Proveedores_model();
			$query = $model->buscarPorRif($datos['rif']);
			if($query->resultID->num_rows > 0){
				return $this->respond(array('message' => 'success', 'data' => $query->getRowArray()), 200);
			}
			else{
				return $this->respond(array('message' => 'not found'), 404);	
			}			
		}
		else{
			return redirect()->to('/403');
		}		
	}

	//Metodo para guardar los datos actualizados del proveedor
	public function editarProveedor(){
		$model = new Proveedores_model();
		if($this->session->get('logged') && $this->request->isAJAX()){
			$datos = json_decode(base64_decode(utf8_decode($this->request->getPost('data'))),TRUE);
			$query = $model->actualizaProveedor($datos);
			if($query){
				return $this->respond(array("message" => "success"),200);
			}
			else{
				return $this->respond(array("message" => "not found"),404);
			}
		}
		else{
			return redirect()->to('/403');
		}
	}

	//Metodo para obtener la tabla nuevamente 
	public function reloadProveedores(){
		$model = new Proveedores_model();
		$rows = array();
		$headings = array("Nº", "RIF", "Nombre", "Telefono Principal", "Telefono Secundario", "Acciones");
		if($this->session->get('logged') && $this->request->isAJAX()){
			$query = $model->getAll();
			if($query->resultID->num_rows > 0){
				foreach($query->getResult() as $row){
					$rows[] = array($row->idprov, $row->numrif, utf8_decode($row->nomprov), $row->telf1, $row->telef2, '<button class="btn btn-sm detalles btn-light" id="'.$row->idprov.'">Detalles</button><button class="btn btn-sm editar btn-primary" id="'.$row->idprov.'">Editar</button>');
				}
			}
			else{
				$rows[] = array('<td colspan="6">Sin registros</td>','','','','','');
			}
			$tabla = base64_encode($this->generarTabla($headings, $rows));
			return $this->respond(array('message' => 'success', 'data' => $tabla),200);
		}
		else{
			return redirect()->to('/403');
		}
	}
}