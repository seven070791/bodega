<?php

namespace App\Controllers;

use App\Models\Productos_model;
use CodeIgniter\API\ResponseTrait;

class Productos extends BaseController
{

	use ResponseTrait;
	/*Metodo que muestra una vista para registrar el producto*/
	public function registrar()
	{
		echo view('template/header');
		echo view('template/nav_bar');
		echo view('productos/regprod');
		echo view('template/footer');
		echo view('productos/footer');
	}

	/*Metodo que registra un nuevo producto*/
	public function addProducto()
	{
		if ($this->request->isAJAX()) {
			$formrequest = json_decode(utf8_encode(base64_decode($this->request->getPost('data'))));
			$data = array(
				'codbar' => $formrequest->codbar,
				'prodmar' => utf8_encode($formrequest->prodmar),
				'prodmodel' => utf8_encode($formrequest->prodmodel),
				'borrado' => utf8_encode($formrequest->borrado),
			);



			$model = new Productos_model();
			if ($formrequest->modform == 1) {
				if (!$model->isProductExists($data['codbar'])) {
					$query = $model->newProd($data);
					if ($query) {
						return $this->respond(array('message' => 'Producto cargado exitosamente'), 200);
					} else {
						return $this->respond(array('message' => 'Producto ya registrado'), 500);
					}
				} else {
					return $this->respond(array('message' => 'Producto ya existe'), 403);
				}
			} else {
				$query = $model->updateProd($data);
				if ($query) {
					return $this->respond(array('message' => 'Producto cargado exitosamente'), 200);
				} else {
					return $this->respond(array('message' => 'Producto ya registrado'), 500);
				}
			}
		} else {
			return redirect()->to('/');
		}
	}

	/*Metodo que muestra la vista para la consulta*/
	public function consutaProducto()
	{
		$model = new Productos_model();
		$tabla = '';
		$query = $model->getAllProd();
		if ($query->resultID->num_rows > 0) {
			foreach ($query->getResult() as $row) {
				if ($row->borrado == "ACTIVO") {
					$tabla .= '<tr><td>' . trim($row->codbar) . '</td><td>' . trim(utf8_decode($row->prodmar)) . '</td><td>' . utf8_decode(trim($row->prodmodel)) . '</td><td style="color: green;">' . trim($row->borrado) . '</td><td><a href="/editarproducto/' . trim($row->codbar) . '">Editar</a></td></tr>';
				} else {
					$tabla .= '<tr><td>' . trim($row->codbar) . '</td><td>' . trim(utf8_decode($row->prodmar)) . '</td><td>' . utf8_decode(trim($row->prodmodel)) . '</td><td style="color: red;">' . trim($row->borrado) . '</td><td><a href="/editarproducto/' . trim($row->codbar) . '">Editar</a></td></tr>';
				}
			}
		} else {
			$tabla .= '<tr><td class="text-center" colspan="4">Sin Registros</td></tr>';
		}
		echo view('template/header');
		echo view('template/nav_bar');
		echo view('productos/content', array('tbody' => $tabla));
		echo view('template/footer');
		echo view('productos/footer');
		unset($model);
	}

	/*Metodo que permite editar un producto*/
	public function show($id = null)
	{
		$model = new Productos_model();
		$query = $model->getSingle($id);
		$data = array();
		if ($query->resultID->num_rows > 0) {
			foreach ($query->getResult() as $row) {
				$data['codbar'] = trim($row->codbar);
				$data['prodmar'] = utf8_decode(trim($row->prodmar));
				$data['prodmodel'] = utf8_decode(trim($row->prodmodel));
				$data['borrado'] = utf8_decode(trim($row->borrado));
			}
			$data['modform'] = 2;
			echo view('template/header');
			echo view('template/nav_bar');
			echo view('productos/editprod', $data);
			echo view('template/footer');
			echo view('productos/footer');
		} else {
			return redirect()->to('/404');
		}
	}

	public function searchByCodbar()
	{
		if ($this->request->isAJAX() && $this->session->get('logged')) {
			$datos = json_decode(utf8_decode(base64_decode($this->request->getPost('data'))), TRUE);
			$model = new Productos_model();
			$query = $model->getSingle($datos['data']);
			$data = array();
			if ($query->resultID->num_rows > 0) {
				foreach ($query->getResult() as $row) {
					$data["codbar"] = trim($row->codbar);
					$data["prodmar"] = trim(utf8_decode($row->prodmar));
					$data["prodmodel"] = trim(utf8_decode($row->prodmodel));
				}
				return $this->respond(array('message' => 'success', 'data' => $data), 200);
			} else {
				return $this->respond(array('message' => 'not found'), 404);
			}
		}
	}
}
