<!-- jQuery -->
<script src="<?php echo base_url();?>/theme/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url();?>/theme/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>/theme/dist/js/adminlte.min.js"></script>
<!-- SweetAlert2 -->
<script src="<?php echo base_url();?>/theme/plugins/sweetalert2/sweetalert2.min.js"></script>
<!--Custom Scripts-->
<script src="<?php echo base_url();?>/custom/js/set-new-pass.js"></script>
</body>
</html>